package com.sidebar.sdklibrary.sidebar.AllViewProperties

import android.graphics.Typeface
import android.webkit.WebSettings

data class SideBarAccordionContentHtmlProperties(var lineViewColor: Int?, var titleTextSize: Float?)