package com.sidebar.sdklibrary.sidebar

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.os.CountDownTimer
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.webkit.WebView
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.dynamicanimation.animation.DynamicAnimation
import androidx.dynamicanimation.animation.SpringAnimation
import androidx.dynamicanimation.animation.SpringForce
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.sidebar.sdklibrary.R
import com.sidebar.sdklibrary.controller.SideBarController
import com.sidebar.sdklibrary.response.HydraMemberTrigger
import com.sidebar.sdklibrary.response.SideBarResponse
import com.sidebar.sdklibrary.response.TriggerSideBarResponse
import com.sidebar.sdklibrary.sidebar.AllViewProperties.*
import com.sidebar.sdklibrary.sidebar.PopUpToast.SideBarPopUp
import com.sidebar.sdklibrary.sidebar.PopUpToast.SideBarToastView
import com.sidebar.sdklibrary.sidebar.PopUpToast.SideBarVideoAndLiveSubjectsPopUp
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainSideBarMenu: RelativeLayout, SideBarController.SideBarListener, SideBarController.TriggerSideBarListener {

    companion object{

        var sideMenuContext:Context? = null

    }

    var activity = AppCompatActivity()
    private var titleView: LinearLayout? = null
    private var contentView: LinearLayout? = null
    private var searchView: SearchView? = null
    private var titleLayout: LinearLayout? = null
    private var titleIcon: ImageView? = null
    private var iconName: TextView? = null
    var emptyText: TextView? = null
    var sideBarMenuIdArray : ArrayList<Long>? = null
    private var sideMenuView: LinearLayout? = null
    var sideBarMenuLinkPopUpProperties: SideBarMenuLinkPopUpProperties? = null
    var sideBarAccordionContentSummaryProperties: SideBarAccordionContentSummaryProperties? = null
    var sideBarAccordionProperties: SideBarAccordionProperties? = null
    var sideBarMainTitleProperties: SideBarMainTitleProperties? = null
    var sideBarSingleAndDoubleColumnProperties: SideBarSingleAndDoubleColumnProperties? = null
    var sideBarSingleDoubleColumnContentImageProperties: SideBarSingleDoubleColumnContentImageProperties? = null
    var sideBarAccordionContentHtmlProperties: SideBarAccordionContentHtmlProperties? = null
    var sideBarPopUpProperties: SideBarPopUpProperties? = null
    var sideBarToastProperties: SideBarToastProperties? = null
    var sideBarController: SideBarController? = null
    var sideBarMenuClickControl = false


    var sideBarVideoAndLiveSubjectsPopUp : SideBarVideoAndLiveSubjectsPopUp? = null
    var sideBarPopUp: SideBarPopUp? = null
    var mainAllProperties: MainAllProperties? = null
    var ad: Dialog? = null
    var sideBarToastPopUp = false
    var mainLayout: LinearLayout? = null
    var scrollView: ScrollView? = null

    var sideBarMemberList: ArrayList<ArrayList<SideBarResponse>>? = null
    var sideBarTriggerDataToastPopUp: TriggerSideBarResponse? = null
    var sideBarTriggerDataCloseToastArray = java.util.ArrayList<HydraMemberTrigger>()

    var filterKey: String? = null
    var filterValue: Int? = null

    var sideMenuFrameSpaceColor: Int? = null


    constructor(context: Context?) : super(context) {
        init(context!!)

    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)

    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)

    }


    fun init(mainContext: Context) {
        sideMenuContext = mainContext

        val view = LayoutInflater.from(sideMenuContext).inflate(R.layout.layout_sidebar_main_menu,this)

        titleView = view.findViewById(R.id.titleView)
        contentView = view.findViewById(R.id.contentView)

        mainLayout = view.findViewById(R.id.mainLayout)
        searchView = view.findViewById(R.id.searchView)
        titleLayout = view.findViewById(R.id.titleLayout)
        emptyText = view.findViewById(R.id.emptyText)
        scrollView = view.findViewById(R.id.scrollView)

        titleIcon = view.findViewById(R.id.titleIcon)
        iconName = view.findViewById(R.id.iconName)

        sideBarVideoAndLiveSubjectsPopUp = SideBarVideoAndLiveSubjectsPopUp(sideMenuContext!!,this)
        sideBarPopUp = SideBarPopUp(sideMenuContext!!,this)

        sideBarMenuIdArray = ArrayList()
        sideBarMemberList = ArrayList()

    }

    fun staticProperties() {
        titleLayoutVisibilityOrSendData(true,R.mipmap.seemeet_icon,"asistan",R.color.black_color,R.color.white_color,ResourcesCompat.getFont(context,R.font.poppins_regular))
        sideBarAccordionContentSummaryProperties = SideBarAccordionContentSummaryProperties(Color.parseColor("#2196F3"),ResourcesCompat.getFont(context,R.font.poppins_bold),Color.parseColor("#def0fd"),ResourcesCompat.getFont(context,R.font.poppins_regular),Color.parseColor("#64B5F6"),24f,18f)
        sideBarAccordionProperties = SideBarAccordionProperties(ContextCompat.getDrawable(sideMenuContext!!,R.drawable.accordion_ligth_blue), ContextCompat.getDrawable(sideMenuContext!!, R.drawable.accordion_blue), Gravity.LEFT, Color.parseColor("#2196F3"), ContextCompat.getColor(sideMenuContext!!,R.color.white_color), R.mipmap.arrow_icon_blue, R.mipmap.arrow_icon_up_white, true,null, Color.parseColor("#64B5F6"), 18f, ResourcesCompat.getFont(context,R.font.poppins_bold))
        sideBarMainTitleProperties = SideBarMainTitleProperties(ContextCompat.getColor(sideMenuContext!!,R.color.black_color), Color.parseColor("#2196F3"), ResourcesCompat.getFont(context,R.font.poppins_bold), 16f)
        sideBarMenuLinkPopUpProperties = SideBarMenuLinkPopUpProperties(Color.parseColor("#2196F3"),ResourcesCompat.getFont(context,R.font.poppins_bold),ContextCompat.getColor(sideMenuContext!!,R.color.black_color),null, Color.parseColor("#64B5F6"),20f,16f)
        sideBarSingleAndDoubleColumnProperties = SideBarSingleAndDoubleColumnProperties(Color.parseColor("#2196F3"),ResourcesCompat.getFont(context,R.font.poppins_bold),Color.parseColor("#64B5F6"),25f)
        sideBarSingleDoubleColumnContentImageProperties = SideBarSingleDoubleColumnContentImageProperties(Color.parseColor("#2196F3"),ResourcesCompat.getFont(context,R.font.poppins_bold),Color.parseColor("#64B5F6"),20f)
        sideBarAccordionContentHtmlProperties = SideBarAccordionContentHtmlProperties(Color.parseColor("#2196F3"),25f)
        sideBarPopUpProperties = SideBarPopUpProperties(ContextCompat.getColor(sideMenuContext!!,R.color.grey_color),150,150,150,150,"#000000","#ffffff",Gravity.RIGHT,null,R.mipmap.close_white,30,Color.parseColor("#64B5F6"))
        sideBarToastProperties = SideBarToastProperties(null,null,R.color.black_color,R.color.black_color,R.color.black_color)
        mainAllProperties = MainAllProperties(sideBarMainTitleProperties,sideBarAccordionProperties,sideBarAccordionContentSummaryProperties,sideBarMenuLinkPopUpProperties,sideBarSingleAndDoubleColumnProperties,sideBarSingleDoubleColumnContentImageProperties,sideBarAccordionContentHtmlProperties,sideBarPopUpProperties,sideBarToastProperties)

    }

    fun sendActivity(activity: AppCompatActivity?,activity2: Activity?){

        if (activity != null){
            this.activity = activity
        }

        if (activity2 != null){
            //this.activity = activity2
        }

    }

    fun setAllViewsProperties(mainAllProperties: MainAllProperties?){

        this.mainAllProperties = mainAllProperties

    }

    fun sideMenuFrameSpaceColorFunc(colorVal: Int?){

        sideMenuFrameSpaceColor = colorVal

    }

    fun clearSideBar(){

        val count = object: CountDownTimer(300,300){
            override fun onFinish() {
                titleView?.removeAllViews()
                contentView?.removeAllViews()
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }
        count.start()

    }

    fun sideBarMenuFirstApiCall(){

        sideBarMenuClickControl = false
        sideBarMenuIdArray = ArrayList()
        sideBarMemberList = ArrayList()
        sideMenuView?.removeAllViews()

        var filterData : JsonObject?

        if (filterKey != null && filterValue != null){

            filterData = JsonObject()

            filterData = filterDataReturn()

            sideBarController = SideBarController(activity,null,"58f98c78-baf9-3bbe-8c1d-c1178d3a4d2a","collections:mobile",0,null,false,filterData,2,"asc",11,this,this)

        }else{

            sideBarController = SideBarController(activity,null,"58f98c78-baf9-3bbe-8c1d-c1178d3a4d2a","collections:mobile",0,null,false,null,2,"asc",11,this,this)

        }

        sideBarController?.sideBar()

    }

    fun pageFilterFunc(key: String,value: Int){

        filterKey = key
        filterValue = value

    }

    fun filterDataReturn() : JsonObject{

        var filterData = JsonObject()

        if (filterKey != null && filterValue != null){

            filterData.addProperty(filterKey,filterValue)

        }

        return filterData

    }


    fun titleLayoutVisibilityOrSendData(visibility: Boolean?,titleIcon: Int?,iconName: String?,iconNameColor: Int?,backgroundColor: Int?,nameTypeface: Typeface?){

        if (visibility != null){

            if (visibility == true){

                titleLayout?.visibility = View.VISIBLE

                if (titleIcon != null){

                    this.titleIcon?.visibility = View.VISIBLE
                    this.titleIcon?.background = resources.getDrawable(titleIcon)

                }

                if (iconName != null){

                    this.iconName?.text = iconName

                }

                if (iconNameColor != null){

                    this.iconName?.setTextColor(iconNameColor)

                }

                if (nameTypeface != null){

                    this.iconName?.typeface = nameTypeface

                }

                if (backgroundColor != null){

                    titleLayout?.setBackgroundColor(ContextCompat.getColor(context,backgroundColor))

                }

            }else{

                titleLayout?.visibility = View.GONE

            }

        }

    }


    fun setTitleData(sideBarMenu: ArrayList<SideBarResponse>?){

        titleView?.removeAllViews()

        if (sideBarMemberList.isNullOrEmpty() || sideBarMemberList?.size == 1){

            if (sideBarMenu == null){

                emptyText?.visibility = View.VISIBLE

            }else{

                val mainView = SideBarMainTitleView(context,activity,this)
                mainView.setTitle("MainPage")
                mainView.setProperties(mainAllProperties?.sideBarMainTitleProperties)
                titleView?.addView(mainView)

            }

        }else{

            val mainView = SideBarMainTitleView(context,activity,this)
            mainView.setTitle(sideBarMenu?.get(0)?.parentCol?.title)
            mainView.setProperties(mainAllProperties?.sideBarMainTitleProperties)
            mainView.setOpenBack()
            titleView?.addView(mainView)

        }

    }


    fun setData(sideBarMenu: ArrayList<SideBarResponse>?, mainAllProperties: MainAllProperties?){

        contentView?.removeAllViews()

        if (sideBarMenu != null){

            for (i in 0..sideBarMenu.size-1){

                when(sideBarMenu.get(i).typeStr){

                    "link" -> {

                        val view = SideBarMenuLinkPopUpView(context,activity,this)
                        view.setProperties(mainAllProperties?.sideBarMenuLinkPopUpProperties)
                        view.setData(sideBarMenu.get(i))
                        contentView?.addView(view)

                    }

                    "menu" -> {

                        val view = SideBarMenuLinkPopUpView(context,activity,this)
                        view.setProperties(mainAllProperties?.sideBarMenuLinkPopUpProperties)
                        view.setData(sideBarMenu.get(i))
                        contentView?.addView(view)

                    }
                    "popup" -> {

                        val view = SideBarMenuLinkPopUpView(context,activity,this)
                        view.setProperties(mainAllProperties?.sideBarMenuLinkPopUpProperties)
                        view.setData(sideBarMenu.get(i))
                        contentView?.addView(view)

                    }

                    "single_column" -> {

                        val view = SideBarSingleAndDoubleColumnView(context,activity,this)
                        view.setProperties(mainAllProperties?.sideBarSingleAndDoubleColumnProperties)
                        view.setTitle(sideBarMenu.get(i).title)
                        view.setDataSideBarMenu(sideBarMenu.get(i).collectionSections,mainAllProperties)
                        contentView?.addView(view)

                    }

                    "double_column" -> {

                        val view = SideBarSingleAndDoubleColumnView(context,activity,this)
                        view.setProperties(mainAllProperties?.sideBarSingleAndDoubleColumnProperties)
                        view.setTitle(sideBarMenu.get(i).title)
                        view.setDataSideBarMenu(sideBarMenu.get(i).collectionSections,mainAllProperties)
                        contentView?.addView(view)

                    }

                    "accordion" -> {

                        val view = SideBarAccordionView(context,this)
                        view.setProperties(mainAllProperties?.sideBarAccordionProperties)
                        view.accordionTitleText(sideBarMenu.get(i).title)
                        view.contentData(sideBarMenu.get(i).collectionSections, mainAllProperties)
                        contentView?.addView(view)

                    }

                }

            }

        }

    }

    fun sideBarToastPopUpp(triggerName: String?,androidLink: String?){

        sideBarTriggerDataCloseToastArray = ArrayList()
        sideBarController?.triggerSideBar(triggerName,"triggers:mobile")

    }

    fun sideBarToastPopUpBase(anyOneActivity: AppCompatActivity?){

        if (sideBarTriggerDataToastPopUp == null){

            sideBarToastPopUp = false

        }else{

            sideBarToastPopUp = sideBarTriggerDataToastPopUp?.hydraMember?.size!! > 0 && sideBarTriggerDataToastPopUp != null

        }

        if (sideBarToastPopUp){

            for (i in 0 until sideBarTriggerDataToastPopUp?.hydraMember?.size!!){

                openToastCloseSideBar()

                if (i == 0){

                    activity.toastLayout.visibility = View.VISIBLE

                }

                val view = SideBarToastView(context, activity, this)
                view.setProperties(mainAllProperties?.sideBarToastProperties)

                view.closeIcon?.setOnClickListener {

                    sideBarTriggerDataCloseToastArray.add(sideBarTriggerDataToastPopUp?.hydraMember?.get(i)!!)

                    if (activity.toastLayout?.childCount!! > 1){

                        activity.toastLayout?.removeView(view)

                    }else{
                        activity.toastLayout?.removeView(view)

                        closeToast(sideBarTriggerDataToastPopUp)

                    }

                    animIn()

                }

                view.setTitleAndSubTitle(sideBarTriggerDataToastPopUp?.hydraMember?.get(0)?.result?.title,sideBarTriggerDataToastPopUp?.hydraMember?.get(0)?.result?.content,sideBarTriggerDataToastPopUp?.hydraMember?.get(0)?.result?.viewTypeStr)

                view.setOnClickListener {

                    animIn()

                    if (activity.toastLayout?.childCount!! > 0){

                        sideBarTriggerDataCloseToastArray.add(sideBarTriggerDataToastPopUp?.hydraMember?.get(i)!!)
                        activity.toastLayout?.removeView(view)

                    }else{

                        activity.toastLayout?.visibility = View.GONE

                    }

                    sideBarTriggerDataToastPopUp = null

                }

                if (sideBarTriggerDataCloseToastArray.size > 0){

                    var closeControl = false

                    for (j in 0..sideBarTriggerDataCloseToastArray.size-1){

                        if (sideBarTriggerDataToastPopUp?.hydraMember?.get(i) == sideBarTriggerDataCloseToastArray.get(j)){

                            closeControl = true

                        }

                    }

                    if (closeControl != true){

                        activity.toastLayout?.addView(view)

                    }

                }else{

                    activity.toastLayout?.addView(view)

                }

            }

        }else{



        }

    }


    fun sideBarMenu(id: Long?){

        sideBarMenuIdArray?.add(id!!)

        sideBarMenuClickControl = true

        var filterData : JsonObject?

        if (filterKey != null && filterValue != null){

            filterData = JsonObject()

            filterData = filterDataReturn()

            sideBarController = SideBarController(activity,null,"58f98c78-baf9-3bbe-8c1d-c1178d3a4d2a","collections:mobile",0,id,null,filterData,2,"asc",11,this,this)

        }else{

            sideBarController = SideBarController(activity,null,"58f98c78-baf9-3bbe-8c1d-c1178d3a4d2a","collections:mobile",0,id,null,null,2,"asc",11,this,this)

        }


        sideBarController?.sideBar()

    }

    fun openToastCloseSideBar(){

        animOut()

    }

    fun closeToast(sideBarTriggerDataToastPopup: TriggerSideBarResponse?){

        if (sideBarTriggerDataToastPopup != null){

        }else{

            activity.toastLayout?.visibility = View.GONE

        }

        if (sideBarTriggerDataToastPopup != null){
            sideBarTriggerDataToastPopUp = null
        }

        ad?.dismiss()

    }

    override fun triggerSideBarListener(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int, error: String) {

        if (responseOk){

            if (jsonElement != null){

                var triggerSideBar = Gson().fromJson(jsonElement, TriggerSideBarResponse::class.java)

                if (triggerSideBar.hydraMember?.size!! > 0){

                    if (triggerSideBar.hydraMember?.get(0)?.result != null){

                        if (triggerSideBar.hydraMember?.get(0)?.result?.typeStr != null){

                            if (triggerSideBar.hydraMember?.get(0)?.result?.typeStr.equals("modal")){

                                //PopUp

                                sideBarPopUp?.setPropertiesPopUp(mainAllProperties?.sideBarPopUpProperties)
                                sideBarPopUp?.setVariables(triggerSideBar,triggerSideBar.hydraMember?.get(0)?.result?.typeStr,triggerSideBar.hydraMember?.get(0)?.result?.viewTypeStr,triggerSideBar?.hydraMember?.get(0)?.result?.linkAndroid)

                                ad = sideBarPopUp

                                ad?.show()
                                ad?.setCanceledOnTouchOutside(true)

                            }else{

                                //Toast

                                sideBarTriggerDataToastPopUp = triggerSideBar

                                sideBarToastPopUpBase(activity)

                            }

                        }

                    }

                }else{



                    if (Locale.getDefault().language.toLowerCase() == "tr"){
                        Toast.makeText(activity,"İçerik Bulunmamaktadır!", Toast.LENGTH_LONG).show()
                    }else{
                        Toast.makeText(activity,"Not Content Found!", Toast.LENGTH_LONG).show()
                    }

                }

            }else{

                if (Locale.getDefault().language.toLowerCase() == "tr"){
                    Toast.makeText(activity,"İçerik Bulunmamaktadır.", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(activity,"Not Content Found!", Toast.LENGTH_LONG).show()
                }

            }

        }else{

            if (Locale.getDefault().language.toLowerCase() == "tr"){
                Toast.makeText(activity,"Servis ile ilgili bir hata oluşmuştur.", Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(activity,"An error has occurred with the service.", Toast.LENGTH_LONG).show()
            }

        }

    }

    fun sideBarTopBack(){

        var lastData = sideBarMemberList?.size?.minus(1)

        if (sideBarMenuClickControl){

            if (sideBarMenuIdArray?.size == 1){
                sideBarMenuClickControl = false
                sideBarMenuIdArray = ArrayList()
            }else{
                sideBarMenuIdArray?.removeAt(sideBarMenuIdArray?.size!!-1)
            }
        }

        sideBarMemberList?.removeAt(lastData!!)

        sideMenuView?.removeAllViews()

        var lastData2 = sideBarMemberList?.size?.minus(1)

        var sideBarResponse = sideBarMemberList?.get(lastData2!!)

        if (sideBarMemberList?.size != 0){
            setTitleData(sideBarResponse)
            setData(sideBarResponse,mainAllProperties)
        }

    }

    override fun sideBarListener(responseOk: Boolean, jsonArray: JsonArray?, failMessage: Int, error: String) {

        if (responseOk){

            if (jsonArray != null){

                val collectionType = object : TypeToken<ArrayList<SideBarResponse>>(){}.type

                var sideBarMenu = ArrayList<SideBarResponse>()

                sideBarMenu = Gson().fromJson(jsonArray,collectionType)

                if (sideBarMenu.isNullOrEmpty()){
                    if (Locale.getDefault().language.toLowerCase() == "tr"){
                        Toast.makeText(activity,"İçerik Bulunmamaktadır.", Toast.LENGTH_LONG).show()
                    }else{
                        Toast.makeText(activity,"Not Content Found!", Toast.LENGTH_LONG).show()
                    }
                }else{

                    sideMenuView?.removeAllViews()

                    sideBarMemberList?.add(sideBarMenu)

                    setTitleData(sideBarMenu)
                    setData(sideBarMenu,mainAllProperties)

                    scrollView?.scrollTo(0,0)

                }

            }else{


            }

        }else{

            setData(null,mainAllProperties)

        }

    }

    fun animIn(){

        this.visibility= View.VISIBLE

        val count = object: CountDownTimer(500,500){
            override fun onFinish() {

                if (sideMenuFrameSpaceColor != null){
                    setBackgroundColor(sideMenuFrameSpaceColor!!)
                }

            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }
        count.start()

        mainLayout?.startAnimation(inFromRightAnimation())
        SpringAnimation(sideMenuView, DynamicAnimation.TRANSLATION_X, 0f).apply {
            this.spring.dampingRatio =  SpringForce.DAMPING_RATIO_LOW_BOUNCY
        }
        sideMenuView?.visibility = View.VISIBLE

    }

    fun animOut(){

        mainLayout?.startAnimation(outToRightAnimation())

        val count = object: CountDownTimer(500,500){
            override fun onFinish() {
                visibility= View.GONE
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }
        count.start()

    }

    fun outToRightAnimation(): Animation {
        val outtoRight = TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f)
        outtoRight.duration = 500

        outtoRight.interpolator = AccelerateInterpolator()
        return outtoRight
    }

    fun inFromRightAnimation(): Animation {
        val inFromRight = TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f)
        inFromRight.duration = 500
        inFromRight.interpolator = AccelerateInterpolator()

        return inFromRight
    }

}