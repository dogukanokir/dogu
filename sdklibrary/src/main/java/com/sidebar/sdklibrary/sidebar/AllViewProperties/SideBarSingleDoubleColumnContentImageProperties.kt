package com.sidebar.sdklibrary.sidebar.AllViewProperties

import android.graphics.Typeface

class SideBarSingleDoubleColumnContentImageProperties(var nameColor: Int?, var typeface: Typeface?, var lineViewColor: Int?, var nameTextSize: Float?)