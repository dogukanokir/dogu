package com.sidebar.sdklibrary.response

import com.google.gson.JsonArray
import com.google.gson.annotations.SerializedName

class SideBarResponse {

    /*@SerializedName("@context")
    val context: String? = null

    @SerializedName("@id")
    val id: String? = null

    @SerializedName("@type")
    val type: String? = null

    @SerializedName("hydra:member")
    val hydraMember: List<HydraMember>? = null

    @SerializedName("hydra:totalItems")
    val hydraTotalItems: Long? = null

    @SerializedName("hydra:view")
    val hydraView: HydraView? = null

    @SerializedName("hydra:search")
    val hydraSearch: HydraSearch? = null*/


    /*@SerializedName("@id")
    val id: String? = null

    @SerializedName("@type")
    val type: String? = null




     */
    @SerializedName("id")
    val hydraMemberID: Long? = null

    val name: String? = null

    val parentCol: parentCol? = null

    val summary: String? = null

    val collectionSections: List<CollectionSection>? = null

    val link: String? = null

    val typeStr: String? = null

    val statusStr: String? = null

    val title: String? = null

    @SerializedName("iconUrl")
    val iconURL: String? = null




    /*@SerializedName("type")
    val hydraMemberType: Long? = null


    val status: Long? = null
    val icon: String? = null
    val showPlace: Long? = null
    val displayOrder: Long? = null
    val conditions: JsonArray? = null
    val site: Site? = null

    val bubble: Any? = null

    val lCollectionArticles: JsonArray? = null
*/



}

class HydraMember {
    @SerializedName("@id")
    val id: String? = null

    @SerializedName("@type")
    val type: String? = null

    @SerializedName("id")
    val hydraMemberID: Long? = null

    val name: String? = null
    val parentCol: parentCol? = null

    @SerializedName("type")
    val hydraMemberType: Long? = null

    val summary: String? = null
    val status: Long? = null
    val icon: String? = null
    val showPlace: Long? = null
    val displayOrder: Long? = null
    val conditions: JsonArray? = null
    val site: Site? = null
    val collectionSections: List<CollectionSection>? = null
    val bubble: Any? = null
    val link: String? = null
    val lCollectionArticles: JsonArray? = null
    val typeStr: String? = null
    val statusStr: String? = null
    val title: String? = null

    @SerializedName("iconUrl")
    val iconURL: String? = null
}

class CollectionSection {
        /*@SerializedName("@id")
        var id: String? = null

        @SerializedName("@type")
        var type: String? = null

         */

        var lsection: Lsection? = null
}

class Lsection {
        /*@SerializedName("@id")
        var id: String? = null

        @SerializedName("@type")
        var type: String? = null

         */

    @SerializedName("id")
    var lsectionID: Long? = null
    var name: String? = null
    @SerializedName("mediaUrl")
    var mediaURL: String? = null
    var html: String? = null
    var summary: String? = null
    var link: String? = null
    var previewImage: String? = null
    var viewTypeStr: String? = null
    var typeStr: String? = null
    var statusStr: String? = null
    var title: String? = null
    var androidLink: String? = null
    var htmlMobile: String? = null

        //@SerializedName("type")
        //var lsectionType: Long? = null
    //var conditions: JsonArray? = null
    // var viewType: Long? = null
    //var status: Long? = null
    //var typeStr: String? = null

}

class parentCol {

    @SerializedName("@id")
    var id: String? = null

    @SerializedName("@type")
    var type: String? = null

    @SerializedName("id")
    var parentColId: Long? = null

    var name: String? = null

    val parentCol: Any? = null

    @SerializedName("type")
    var typeVal: String? = null

    var summary: String? = null
    var status: Long? = null
    var icon: String? = null
    var showPlace: Int? = null
    var displayOrder: Int? = null
    var conditions: JsonArray? = null
    var site: Site? = null
    val collectionSections: List<CollectionSection>? = null
    val bubble: Any? = null
    var link: String? = null
    val lCollectionArticles: JsonArray? = null
    var typeStr: String? = null
    var statusStr: String? = null
    var iconUrl: String? = null
    var title: String? = null

}

class Site {
        @SerializedName("@id")
        var id: String? = null

        @SerializedName("@type")
        var type: String? = null

        @SerializedName("id")
        var siteID: Long? = null
}

class HydraSearch {
    @SerializedName("@type")
    var type: String? = null

    @SerializedName("hydra:template")
    var hydraTemplate: String? = null

    @SerializedName("hydra:variableRepresentation")
    var hydraVariableRepresentation: String? = null

    @SerializedName("hydra:mapping")
    var hydraMapping: List<HydraMapping>? = null
}

class HydraMapping {
    @SerializedName("@type")
    var type: String? = null
    var variable: String? = null
    var property: String? = null
    var required: Boolean? = null
}

class HydraView {
    @SerializedName("@id")
    var id: String? = null

    @SerializedName("@type")
    var type: String? = null
}
