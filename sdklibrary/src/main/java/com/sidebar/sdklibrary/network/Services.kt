package com.sidebar.sdklibrary.network


import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody

import retrofit2.Call
import retrofit2.http.*
import java.util.*

interface Services {

    //SideBarApi

    interface GetSideBar {
        @Headers("Accept: application/json")
        @GET("collections")
        fun getSideBar(@Header("apiKey") apiKey: String?, @Query("groups[]") groups: String?, @Query("showPlace") showPlace: Int?, @Query("parentCol") parentCol: Long?, @Query("exists[parentCol]") exists : Boolean?, @Query("filter") filter : JsonObject?, @Query("status") status : Int?, @Query("order[displayOrder]") order : String?, @Query("site") site : Int?) : Call<JsonArray>
    }

    interface GetTriggerSideBar {
        @GET("triggers")
        fun getTriggerSideBar(@Query("name") name: String?,@Query("groups[]") groups: String?) : Call<JsonElement>
    }

}