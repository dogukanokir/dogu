package com.sidebar.sidebarsdkversion

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.core.content.res.ResourcesCompat
import com.sidebar.sdklibrary.sidebar.MainSideBarMenu

class MainActivity : AppCompatActivity() {

    var sideMenuFrame: MainSideBarMenu? = null
    private var sideMenuIcon: Button? = null
    var toastLayout: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sideMenuFrame = findViewById(R.id.sideMenuFrame)
        sideMenuIcon = findViewById(R.id.sideMenuIcon)
        toastLayout = findViewById(R.id.toastLayout)

        sideMenuFrame?.staticProperties()
        sideMenuFrame?.sideMenuFrameSpaceColorFunc(R.color.black_color)
        //sideMenuFrame?.setAllViewsProperties(null)

        sideMenuFrame?.pageFilterFunc("pageUrl",13)

        sideMenuIcon?.setOnClickListener {

            sideMenuFrame?.visibility= View.VISIBLE
            sideMenuFrame?.sendActivity(this,null)
            sideMenuFrame?.sideBarMenuFirstApiCall()
            sideMenuFrame?.animIn()

        }

        sideMenuFrame?.setOnClickListener {

            sideMenuFrame?.clearSideBar()
            sideMenuFrame?.animOut()

        }

    }

}
